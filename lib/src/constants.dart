import 'package:flutter/material.dart';

const kTmdbUrl = 'api.themoviedb.org';
const Map<String, String> kTmdbApiKey = {
  'api_key': '1b869b3ccf57d089047ded4b1de007b8',
  'language': 'en-US'
};
const kImageBaseUrl = 'http://image.tmdb.org/t/p/';

const kGrey10 = Color(0xFFF4F4F4);
const kTextGrey = Color(0xFF838383);

const kWhite = Color(0xFFfafafa);
const kPrimaryColor = Color(0xFFda1a37);
const kSecondaryColor = Color(0xFF1a1a2c);
