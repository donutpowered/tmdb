import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:tmdb/src/models/failure.dart';
import 'package:tmdb/src/models/genre_list.dart';
import 'package:tmdb/src/models/movie_detail.dart';
import 'package:tmdb/src/models/movie_list.dart';
import 'package:tmdb/src/repositories/movie_repository.dart';

part 'movie_list_state.dart';

class MovieListCubit extends Cubit<MovieListState> {
  MovieListCubit() : super(const MovieListState());

  final MovieRepository _movieRepo = MovieRepository();

  Future<void> getMovieList({Map<String, String>? params}) async {
    emit(state.copyWith(status: MovieListStatus.loading));

    try {
      if (params == null) {
        params = <String, String>{};
      }

      // final Map<String, dynamic> params = {'include_video': 'false'};

      final MovieList? _movies = await _movieRepo.discoverMovies(params);
      final GenreList? _genreIdx = await _movieRepo.getGenreList();

      List<String> _genreTextList = [];
      if (_movies != null && _movies.results != null) {
        for (var movie in _movies.results!) {
          List<String> temp = [];

          if (movie.genreIds.isEmpty) {
            temp.add('N/A');
          }

          for (var movieGenreId in movie.genreIds) {
            var genreFound = _genreIdx!.genres!.firstWhere(
                (genre) => genre.id == movieGenreId,
                orElse: () => Genres(id: -1, name: 'N/A'));

            if (genreFound.name.isNotEmpty) {
              temp.add(genreFound.name);
            }
          }

          if (temp.isNotEmpty) {
            final formattedGenre = temp.join(', ');
            _genreTextList.add(formattedGenre);
          }
        }
      }

      // print(_genreTextList[1]);
      emit(state.copyWith(
          status: MovieListStatus.success,
          movieList: _movies,
          genreTextList: _genreTextList));
    } on Failure catch (f) {
      emit(state.copyWith(status: MovieListStatus.failure, failure: f));
    }
  }
}
