part of 'movie_detail_cubit.dart';

enum MovieDetailStatus { initial, loading, success, failure }

extension MovieDetailStatusX on MovieDetailStatus {
  bool get isInitial => this == MovieDetailStatus.initial;
  bool get isLoading => this == MovieDetailStatus.loading;
  bool get isSuccess => this == MovieDetailStatus.success;
  bool get isFailure => this == MovieDetailStatus.failure;
}

class MovieDetailState extends Equatable {
  const MovieDetailState(
      {this.status = MovieDetailStatus.initial,
      this.movieDetail,
      this.genreText,
      this.runtimeText,
      this.spokenLanguagesText,
      this.failure});

  final MovieDetailStatus status;
  final MovieDetail? movieDetail;
  final String? genreText;
  final String? runtimeText;
  final String? spokenLanguagesText;
  final Failure? failure;

  @override
  List<Object?> get props => [status, movieDetail, failure];

  MovieDetailState copyWith({
    MovieDetailStatus? status,
    MovieDetail? movieDetail,
    String? genreText,
    String? runtimeText,
    String? spokenLanguagesText,
    Failure? failure,
  }) {
    return MovieDetailState(
      status: status ?? this.status,
      movieDetail: movieDetail ?? this.movieDetail,
      genreText: genreText ?? this.genreText,
      runtimeText: runtimeText ?? this.runtimeText,
      spokenLanguagesText: spokenLanguagesText ?? this.spokenLanguagesText,
      failure: failure ?? this.failure,
    );
  }
}
