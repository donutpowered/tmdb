import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:tmdb/src/models/failure.dart';
import 'package:tmdb/src/models/fav_movie.dart';
import 'package:tmdb/src/repositories/local_repository.dart';

part 'favorite_movie_state.dart';

class FavoriteMovieCubit extends Cubit<FavoriteMovieState> {
  FavoriteMovieCubit() : super(const FavoriteMovieState());

  final LocalRepository _localRepo = LocalRepository();

  Future<void> getFavoriteMovies() async {
    emit(state.copyWith(status: FavMovieStatus.loading));

    try {
      final _movies = await _localRepo.getFavoriteMovies();
      _movies.sort((a, b) => b.indexing.compareTo(a.indexing));

      emit(state.copyWith(status: FavMovieStatus.success, favMovies: _movies));
    } on Failure catch (f) {
      emit(state.copyWith(status: FavMovieStatus.failure, failure: f));
    }
  }

  Future<void> checkFavMovie(int movieId) async {
    emit(state.copyWith(status: FavMovieStatus.loading));
    final isFavorite = await _localRepo.isFavoriteMovie(movieId);

    if (isFavorite) {
      // print('yes favorited $movieId');
      emit(state.copyWith(status: FavMovieStatus.favorite));
    } else {
      emit(state.copyWith(status: FavMovieStatus.notFavorite));
    }
  }

  Future<void> toggleFavMovie(FavMovie movie, bool isFavorite) async {
    if (isFavorite) {
      await _localRepo.unsetFavoriteMovie(movie.id);

      var _movies = await _localRepo.getFavoriteMovies();
      _movies.sort((a, b) => a.indexing.compareTo(b.indexing));
      for (int i = 0; i < _movies.length; i++) {
        _movies[i].indexing = i;
      }

      await _localRepo.saveFavoriteList(_movies);
    } else {
      movie.indexing = await _localRepo.getSizeFavoriteList();

      await _localRepo.setFavoriteMovie(movie);
    }
  }

  Future<void> saveReorderedFavList(List<FavMovie>? movies) async {
    emit(state.copyWith(status: FavMovieStatus.loading));

    var reversed = List<FavMovie>.from(movies!.reversed);
    for (int i = 0; i < reversed.length; i++) {
      reversed[i].indexing = i;
    }
    await _localRepo.saveFavoriteList(reversed);
    emit(state.copyWith(status: FavMovieStatus.success, favMovies: reversed));
  }
}
