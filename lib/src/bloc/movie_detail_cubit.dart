import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:tmdb/src/models/failure.dart';
import 'package:tmdb/src/models/movie_detail.dart';
import 'package:tmdb/src/repositories/movie_repository.dart';

part 'movie_detail_state.dart';

class MovieDetailCubit extends Cubit<MovieDetailState> {
  MovieDetailCubit() : super(const MovieDetailState());

  final MovieRepository _movieRepo = MovieRepository();

  Future<void> getMovieDetail(int movieId) async {
    emit(state.copyWith(status: MovieDetailStatus.loading));

    try {
      final MovieDetail? _movieDetail =
          await _movieRepo.getMovieDetails(movieId);

      String formattedGenre = 'N/A';
      if (_movieDetail != null && _movieDetail.genres!.isNotEmpty) {
        List<String> temp = [];
        for (var genre in _movieDetail.genres!) {
          temp.add(genre.name);
        }
        formattedGenre = temp.join(', ');
      }

      String spokenLangText = '';
      if (_movieDetail != null && _movieDetail.spokenLanguages!.isNotEmpty) {
        List<String> temp = [];
        for (var lang in _movieDetail.spokenLanguages!) {
          temp.add(lang.englishName);
        }
        spokenLangText = temp.join(', ');
      }

      final runtime = Duration(minutes: _movieDetail!.runtime);
      final runtimeText =
          '${runtime.inHours}h ${runtime.inMinutes.remainder(60)}m ';

      emit(state.copyWith(
          status: MovieDetailStatus.success,
          movieDetail: _movieDetail,
          genreText: formattedGenre,
          runtimeText: runtimeText,
          spokenLanguagesText: spokenLangText));
    } on Failure catch (f) {
      // print("failed getting details");
      emit(state.copyWith(status: MovieDetailStatus.failure));
    }
  }
}
