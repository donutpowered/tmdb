part of 'movie_list_cubit.dart';

enum MovieListStatus { initial, loading, success, failure }

extension MovieListStatusX on MovieListStatus {
  bool get isInitial => this == MovieListStatus.initial;
  bool get isLoading => this == MovieListStatus.loading;
  bool get isSuccess => this == MovieListStatus.success;
  bool get isFailure => this == MovieListStatus.failure;
}

class MovieListState extends Equatable {
  const MovieListState(
      {this.status = MovieListStatus.initial,
      this.movieList,
      this.genreTextList,
      this.failure});

  final MovieListStatus status;
  final MovieList? movieList;
  final List<String>? genreTextList;
  final Failure? failure;

  @override
  List<Object?> get props => [status, movieList, genreTextList, failure];

  MovieListState copyWith({
    MovieListStatus? status,
    MovieList? movieList,
    List<String>? genreTextList,
    Failure? failure,
  }) {
    return MovieListState(
      status: status ?? this.status,
      movieList: movieList ?? this.movieList,
      genreTextList: genreTextList ?? this.genreTextList,
      failure: failure ?? this.failure,
    );
  }
}
