part of 'favorite_movie_cubit.dart';

enum FavMovieStatus {
  initial,
  loading,
  success,
  failure,
  favorite,
  notFavorite
}

extension FavMovieStatusX on FavMovieStatus {
  bool get isInitial => this == FavMovieStatus.initial;
  bool get isLoading => this == FavMovieStatus.loading;
  bool get isSuccess => this == FavMovieStatus.success;
  bool get isFailure => this == FavMovieStatus.failure;
  bool get isFavorite => this == FavMovieStatus.favorite;
  bool get isNotFavorite => this == FavMovieStatus.notFavorite;
}

class FavoriteMovieState extends Equatable {
  const FavoriteMovieState(
      {this.status = FavMovieStatus.initial, this.favMovies, this.failure});

  final FavMovieStatus status;
  final List<FavMovie>? favMovies;
  final Failure? failure;

  @override
  List<Object?> get props => [status, favMovies, failure];

  FavoriteMovieState copyWith({
    FavMovieStatus? status,
    List<FavMovie>? favMovies,
    Failure? failure,
  }) {
    return FavoriteMovieState(
      status: status ?? this.status,
      favMovies: favMovies ?? this.favMovies,
      failure: failure ?? this.failure,
    );
  }
}
