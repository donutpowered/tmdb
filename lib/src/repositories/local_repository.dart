import 'package:hive/hive.dart';
import 'package:tmdb/src/models/fav_movie.dart';

class LocalRepository {
  static final LocalRepository _instance = LocalRepository._internal();
  factory LocalRepository() => _instance;
  LocalRepository._internal();

  Future<bool> isFavoriteMovie(int movieId) async {
    final favBox = await Hive.openBox('favBox');
    return favBox.containsKey(movieId);
  }

  Future<List<FavMovie>> getFavoriteMovies() async {
    final favBox = await Hive.openBox('favBox');
    final movieIds = favBox.keys;

    List<FavMovie> favMovies = [];
    for (var movieId in movieIds) {
      final movie = favBox.get(movieId);
      if (movie != null) {
        favMovies.add(movie);
      }
    }

    return favMovies;
  }

  Future<void> setFavoriteMovie(FavMovie favMovie) async {
    final favBox = await Hive.openBox('favBox');
    await favBox.put(favMovie.id, favMovie);
  }

  Future<void> unsetFavoriteMovie(int movieId) async {
    final favBox = await Hive.openBox('favBox');
    await favBox.delete(movieId);
  }

  Future<void> saveFavoriteList(List<FavMovie> movies) async {
    final favBox = await Hive.openBox('favBox');

    for (var movie in movies) {
      await favBox.put(movie.id, movie);
    }
  }

  Future<int> getSizeFavoriteList() async {
    final favBox = await Hive.openBox('favBox');
    return favBox.length;
  }
}
