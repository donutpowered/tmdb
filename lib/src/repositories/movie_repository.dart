import 'package:tmdb/src/api/tmdb_api.dart';
import 'package:tmdb/src/models/failure.dart';
import 'package:tmdb/src/models/genre_list.dart';
import 'package:tmdb/src/models/movie_detail.dart';
import 'package:tmdb/src/models/movie_list.dart';

class MovieRepository {
  static final MovieRepository _instance = MovieRepository._internal();
  factory MovieRepository() => _instance;
  MovieRepository._internal();

  final TmdbApi apiService = TmdbApi();

  Future<MovieList> discoverMovies(Map<String, String> params) async {
    const String _endpoint = '/3/discover/movie';

    try {
      return await apiService.tmdbGet(MovieList(), _endpoint, params: params);
    } catch (e) {
      throw Failure(e.toString());
    }
  }

  Future<GenreList> getGenreList() async {
    const String _endpoint = '/3/genre/movie/list';

    try {
      return await apiService.tmdbGet(GenreList(), _endpoint);
    } catch (e) {
      throw Failure(e.toString());
    }
  }

  Future<MovieDetail> getMovieDetails(int movieId) async {
    String _endpoint = '3/movie/$movieId';

    try {
      return await apiService.tmdbGet(MovieDetail(), _endpoint);
    } catch (e) {
      print("failed getting details: $e");
      throw Failure(e.toString());
    }
  }
}
