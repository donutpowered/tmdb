import 'package:tmdb/src/models/api_object.dart';

import 'movie_detail.dart';

class GenreList extends ApiObject<GenreList> {
  GenreList({
    this.genres,
  });

  final List<Genres>? genres;

  @override
  GenreList fromJson(Map<String, dynamic> json) => GenreList(
      genres:
          List.from(json['genres']).map((e) => Genres.fromJson(e)).toList());
}
