abstract class ApiObject<T> {
  T fromJson(Map<String, dynamic> json);
}
