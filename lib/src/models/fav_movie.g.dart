// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fav_movie.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class FavMovieAdapter extends TypeAdapter<FavMovie> {
  @override
  final int typeId = 0;

  @override
  FavMovie read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return FavMovie(
      fields[0] as int,
      fields[1] as String,
      fields[2] as String,
      fields[3] as String,
      fields[4] as double,
      fields[5] as int,
    );
  }

  @override
  void write(BinaryWriter writer, FavMovie obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.title)
      ..writeByte(2)
      ..write(obj.posterPath)
      ..writeByte(3)
      ..write(obj.overview)
      ..writeByte(4)
      ..write(obj.popularity)
      ..writeByte(5)
      ..write(obj.indexing);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FavMovieAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
