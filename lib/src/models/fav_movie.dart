import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';

part 'fav_movie.g.dart';

@HiveType(typeId: 0)
class FavMovie extends Equatable {
  FavMovie(this.id, this.title, this.posterPath, this.overview, this.popularity,
      this.indexing);

  @HiveField(0)
  final int id;

  @HiveField(1)
  final String title;

  @HiveField(2)
  final String posterPath;

  @HiveField(3)
  final String overview;

  @HiveField(4)
  final double popularity;

  @HiveField(5)
  int indexing = 0;

  @override
  List<Object?> get props => [id, indexing];
}
