import 'package:tmdb/src/models/api_object.dart';
import 'package:tmdb/src/models/fav_movie.dart';

class MovieDetail extends ApiObject<MovieDetail> implements FavMovie {
  MovieDetail({
    this.adult,
    this.backdropPath = '',
    this.budget = 0,
    this.genres,
    this.homepage = '',
    this.id = 0,
    this.imdbId = '',
    this.originalLanguage,
    this.originalTitle,
    this.overview = '',
    this.popularity = 0.0,
    this.posterPath = '',
    this.productionCompanies,
    this.productionCountries,
    this.releaseDate = '',
    this.revenue,
    this.runtime = 0,
    this.spokenLanguages,
    this.status,
    this.tagline = '',
    this.title = '',
    this.video,
    this.voteAverage,
    this.voteCount,
  });
  late final bool? adult;
  late final String? backdropPath;
  late final int? budget;
  late final List<Genres>? genres;
  late final String? homepage;
  late final int id;
  late final String? imdbId;
  late final String? originalLanguage;
  late final String? originalTitle;
  late final String overview;
  late final double popularity;
  late final String posterPath;
  late final List<ProductionCompanies>? productionCompanies;
  late final List<dynamic>? productionCountries;
  late final String releaseDate;
  late final int? revenue;
  late final int runtime;
  late final List<SpokenLanguages>? spokenLanguages;
  late final String? status;
  late final String? tagline;
  late final String title;
  late final bool? video;
  late final double? voteAverage;
  late final int? voteCount;

  @override
  MovieDetail fromJson(Map<String, dynamic> json) => MovieDetail(
        adult: json['adult'],
        backdropPath: json['backdrop_path'] ?? '',
        budget: json['budget'],
        genres:
            List.from(json['genres']).map((e) => Genres.fromJson(e)).toList(),
        homepage: json['homepage'] ?? '',
        id: json['id'],
        imdbId: json['imdb_id'] ?? '',
        originalLanguage: json['original_language'],
        originalTitle: json['original_title'],
        overview: json['overview'] ?? '',
        popularity: json['popularity'].toDouble() ?? 0.0,
        posterPath: json['poster_path'] ?? '',
        productionCompanies: List.from(json['production_companies'])
            .map((e) => ProductionCompanies.fromJson(e))
            .toList(),
        productionCountries:
            List.castFrom<dynamic, dynamic>(json['production_countries']),
        releaseDate: json['release_date'],
        revenue: json['revenue'],
        runtime: json['runtime'] ?? 0,
        spokenLanguages: List.from(json['spoken_languages'])
            .map((e) => SpokenLanguages.fromJson(e))
            .toList(),
        status: json['status'],
        tagline: json['tagline'] ?? '',
        title: json['title'],
        video: json['video'],
        voteAverage: json['vote_average'].toDouble() ?? 0.0,
        voteCount: json['vote_count'],
      );

  @override
  int indexing = 0;

  @override
  List<Object?> get props => [id];

  @override
  bool? get stringify => true;
}

class Genres {
  Genres({
    required this.id,
    required this.name,
  });
  late final int id;
  late final String name;

  Genres.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['name'] = name;
    return _data;
  }
}

class ProductionCompanies {
  ProductionCompanies({
    required this.id,
    this.logoPath = '',
    required this.name,
    required this.originCountry,
  });
  late final int id;
  late final String logoPath;
  late final String name;
  late final String originCountry;

  ProductionCompanies.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    logoPath = json['logo_path'] ?? '';
    name = json['name'];
    originCountry = json['origin_country'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['logo_path'] = logoPath;
    _data['name'] = name;
    _data['origin_country'] = originCountry;
    return _data;
  }
}

class SpokenLanguages {
  SpokenLanguages({
    required this.englishName,
    required this.iso_639_1,
    required this.name,
  });
  late final String englishName;
  late final String iso_639_1;
  late final String name;

  SpokenLanguages.fromJson(Map<String, dynamic> json) {
    englishName = json['english_name'];
    iso_639_1 = json['iso_639_1'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['english_name'] = englishName;
    _data['iso_639_1'] = iso_639_1;
    _data['name'] = name;
    return _data;
  }
}
