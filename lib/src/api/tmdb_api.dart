import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:tmdb/src/models/api_object.dart';
import 'package:tmdb/src/models/failure.dart';

import '../constants.dart';

class TmdbApi {
  static final TmdbApi _instance = TmdbApi._internal();
  factory TmdbApi() => _instance;
  TmdbApi._internal();

  Future<R> tmdbGet<T extends ApiObject<dynamic>, R>(T obj, String endpoint,
      {Map<String, String>? params}) async {
    params ??= <String, String>{};
    params.addAll(kTmdbApiKey);

    // final apiUri = Uri.parse(
    //     'https://api.themoviedb.org/3/discover/movie?api_key=1b869b3ccf57d089047ded4b1de007b8&language=en-US');

    final apiUri = Uri.https(kTmdbUrl, endpoint, params);

    print('URI: ${apiUri.authority}');
    print('URI: ${apiUri.path}');
    print('URI: ${apiUri.queryParameters}');

    final dynamic parsedJson;
    final http.Response apiResult;

    try {
      apiResult = await http.get(
        apiUri,
      );
    } on SocketException {
      throw Failure('No Internet Connection');
    }

    if (apiResult.statusCode != 200) {
      throw Failure('Endpoint URL Error: ${apiResult.reasonPhrase!}');
    }

    parsedJson = json.decode(apiResult.body);
    // print('endpoint called');
    // print(parsedJson);

    if (parsedJson is! Map) {
      final List<T> tList = <T>[];
      parsedJson.forEach((dynamic data) {
        tList.add(obj.fromJson(data as dynamic));
      });

      return tList as R;
    } else {
      return obj.fromJson(parsedJson as Map<String, dynamic>);
    }
  }
}
