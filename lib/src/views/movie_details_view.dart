import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:tmdb/src/bloc/favorite_movie_cubit.dart';
import 'package:tmdb/src/bloc/movie_detail_cubit.dart';
import 'package:tmdb/src/constants.dart';
import 'package:tmdb/src/views/homepage_webview.dart';

import 'common/favorite_button.dart';

class MovieDetailsView extends StatelessWidget {
  const MovieDetailsView({Key? key, required this.movieId}) : super(key: key);

  static const routeName = '/movie_details';

  final int movieId;

  @override
  Widget build(BuildContext context) {
    // print('Movie ID: $movieId');

    return BlocProvider(
      create: (context) => MovieDetailCubit()..getMovieDetail(movieId),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          title: const Text('Movie Detail'),
        ),
        extendBodyBehindAppBar: true,
        body: BlocBuilder<MovieDetailCubit, MovieDetailState>(
          builder: (context, state) {
            if (state.status.isLoading) {
              return Container(
                height: 350,
                alignment: Alignment.center,
                color: Colors.white,
                child: const CircularProgressIndicator(),
              );
            }

            if (state.movieDetail == null) {
              return Container(
                height: 350.0,
                padding: const EdgeInsets.symmetric(horizontal: 44.0),
                color: Colors.white,
                alignment: Alignment.center,
                child: const Text(
                  'Unable to Fetch Movie Details',
                  textAlign: TextAlign.center,
                ),
              );
            }

            final movie = state.movieDetail;
            final genreText = state.genreText;
            final runtimeText = state.runtimeText;
            final spokenLanguagesText = state.spokenLanguagesText;

            return SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 600,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: CachedNetworkImageProvider(
                              '${kImageBaseUrl}w500${movie!.backdropPath}'),
                          fit: BoxFit.cover),
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                        colors: [
                          kWhite,
                          kWhite.withOpacity(0.8),
                          kWhite.withOpacity(0.5),
                          Colors.transparent
                        ],
                        begin: Alignment.bottomCenter,
                        end: Alignment.topCenter,
                      )),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 15, right: 15),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Center(
                              child: CachedNetworkImage(
                                width: 180,
                                imageUrl:
                                    '${kImageBaseUrl}w220_and_h330_face${movie.posterPath}',
                                fit: BoxFit.cover,
                                placeholder: (context, _) => const Icon(
                                  Icons.image,
                                  size: 60,
                                  color: kGrey10,
                                ),
                                errorWidget: (context, _, error) =>
                                    const Center(
                                  child: Icon(
                                    Icons.error,
                                    size: 30,
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(height: 5),
                            Text(
                              movie.title,
                              style: Theme.of(context).textTheme.headline6,
                              textAlign: TextAlign.center,
                            ),
                            const SizedBox(height: 5),
                            const Divider(thickness: 4),
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Text(
                                            DateFormat.yMMMd().format(
                                                DateTime.parse(
                                                    movie.releaseDate)),
                                          ),
                                          const Padding(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 8.0),
                                            child: Icon(Icons.circle, size: 12),
                                          ),
                                          Text('$runtimeText'),
                                          const Padding(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 8.0),
                                            child: Icon(Icons.circle, size: 12),
                                          ),
                                          Text(
                                            NumberFormat.compactSimpleCurrency(
                                                    locale: 'en-US')
                                                .format(movie.revenue),
                                          ),
                                        ],
                                      ),
                                      Text('$genreText'),
                                      Text('Languages: $spokenLanguagesText'),
                                      Text('Popularity: ${movie.popularity}'),
                                      const SizedBox(height: 5),
                                      Row(
                                        children: [
                                          Text(
                                            movie.voteAverage.toString(),
                                            style: TextStyle(
                                              color: Colors.amber[900],
                                              fontSize: 17,
                                            ),
                                          ),
                                          const SizedBox(width: 5),
                                          ...List.generate(
                                            5,
                                            (index) => Icon(
                                              Icons.star,
                                              color: (index <
                                                      (movie.voteAverage! / 2)
                                                          .floor())
                                                  ? Colors.amber[900]
                                                  : Colors.grey,
                                            ),
                                          ),
                                          const SizedBox(width: 5),
                                          Text('${movie.voteCount} votes'),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: BlocProvider(
                                    create: (context) => FavoriteMovieCubit()
                                      ..checkFavMovie(movie.id),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Tooltip(
                                            message: 'Mark as Favorite',
                                            child:
                                                FavoriteButton(movie: movie)),
                                        Tooltip(
                                          message: 'Movie Website',
                                          child: IconButton(
                                            icon: const Icon(Icons.link),
                                            color: Colors.blue[800],
                                            onPressed: () {
                                              Navigator.pushNamed(context,
                                                  HomepageWebview.routeName,
                                                  arguments: movie.homepage);
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15, top: 20),
                    child: Text(
                      "Overview",
                      style: Theme.of(context).textTheme.headline6,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 35, top: 10, right: 35, bottom: 20),
                    child: Text(
                      movie.overview,
                    ),
                  ),
                  GridView.count(
                    crossAxisCount: 2,
                    // childAspectRatio: 16 / 9,
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    children: List.generate(movie.productionCompanies!.length,
                        (index) {
                      final company = movie.productionCompanies![index];

                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 35),
                        child: Column(
                          children: [
                            CachedNetworkImage(
                              width: 80,
                              imageUrl:
                                  '${kImageBaseUrl}h50${company.logoPath}',
                              fit: BoxFit.cover,
                              placeholder: (context, _) => const Icon(
                                Icons.image,
                                size: 40,
                                color: kGrey10,
                              ),
                              errorWidget: (context, _, error) => const Center(
                                child: Icon(
                                  Icons.error,
                                  size: 40,
                                ),
                              ),
                            ),
                            const SizedBox(height: 20),
                            Text(
                              company.name,
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                          ],
                        ),
                      );
                    }),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
