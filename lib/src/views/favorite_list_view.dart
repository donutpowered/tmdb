import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tmdb/src/bloc/favorite_movie_cubit.dart';

import '../constants.dart';
import 'movie_details_view.dart';

class FavoriteListView extends StatefulWidget {
  const FavoriteListView({Key? key}) : super(key: key);

  static const routeName = '/favorite_list';

  @override
  State<FavoriteListView> createState() => _FavoriteListViewState();
}

class _FavoriteListViewState extends State<FavoriteListView> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => FavoriteMovieCubit()..getFavoriteMovies(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Favorite List'),
        ),
        body: BlocBuilder<FavoriteMovieCubit, FavoriteMovieState>(
          builder: (context, state) {
            if (state.status.isLoading) {
              return Container(
                height: 350,
                alignment: Alignment.center,
                color: Colors.white,
                child: const CircularProgressIndicator(),
              );
            }

            if (state.favMovies == null || state.favMovies!.isEmpty) {
              return Container(
                height: 350.0,
                padding: const EdgeInsets.symmetric(horizontal: 44.0),
                color: Colors.white,
                alignment: Alignment.center,
                child: const Text(
                  'No Favorite Movie Found',
                  textAlign: TextAlign.center,
                ),
              );
            }

            return WillPopScope(
              onWillPop: () async {
                if (state.favMovies != null && state.favMovies!.isNotEmpty) {
                  await context
                      .read<FavoriteMovieCubit>()
                      .saveReorderedFavList(state.favMovies);
                }
                return true;
              },
              child: ReorderableListView.builder(
                restorationId: 'favoriteMovieListView',
                itemCount: state.favMovies!.length,
                onReorder: (int oldIndex, int newIndex) {
                  setState(() {
                    if (oldIndex < newIndex) {
                      newIndex -= 1;
                    }
                    final item = state.favMovies!.removeAt(oldIndex);
                    state.favMovies!.insert(newIndex, item);
                  });
                },
                itemBuilder: (BuildContext context, int index) {
                  final movie = state.favMovies![index];

                  return InkWell(
                    key: Key('$index'),
                    onTap: () {
                      Navigator.pushNamed(
                        context,
                        MovieDetailsView.routeName,
                        arguments: movie.id,
                      ).then((value) => context
                          .read<FavoriteMovieCubit>()
                          .getFavoriteMovies());
                    },
                    child: Card(
                      margin: const EdgeInsets.only(
                          left: 14.0, right: 14.0, bottom: 4.0),
                      child: Row(
                        children: [
                          CachedNetworkImage(
                            width: 60,
                            imageUrl:
                                '${kImageBaseUrl}w220_and_h330_face${movie.posterPath}',
                            fit: BoxFit.cover,
                            placeholder: (context, _) => const Icon(
                              Icons.image,
                              size: 60,
                              color: kGrey10,
                            ),
                            errorWidget: (context, _, error) => const Center(
                              child: Icon(
                                Icons.error,
                                size: 30,
                              ),
                            ),
                          ),
                          Expanded(
                            child: ListTile(
                              isThreeLine: true,
                              title: Text(movie.title,
                                  style: Theme.of(context).textTheme.bodyText1),
                              subtitle: Text(
                                movie.overview,
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                style: Theme.of(context).textTheme.caption,
                              ),
                              trailing: ReorderableDragStartListener(
                                index: index,
                                child: const Icon(Icons.drag_handle),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
