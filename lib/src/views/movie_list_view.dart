import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tmdb/src/bloc/favorite_movie_cubit.dart';
import 'package:tmdb/src/bloc/movie_list_cubit.dart';
import 'package:tmdb/src/constants.dart';
import 'package:tmdb/src/models/movie_list.dart';
import 'package:tmdb/src/views/favorite_list_view.dart';
import 'package:tmdb/src/views/movie_details_view.dart';
import 'package:tmdb/src/views/sort_filter_view.dart';

import '../settings/settings_view.dart';
import 'common/favorite_button.dart';

class MovieListView extends StatelessWidget {
  const MovieListView({
    Key? key,
  }) : super(key: key);

  static const routeName = '/';

  @override
  Widget build(BuildContext context) {
    final movieListCubit = MovieListCubit();

    return BlocProvider.value(
      value: movieListCubit..getMovieList(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Movie List'),
          actions: [
            IconButton(
                onPressed: () async {
                  await Navigator.pushNamed(
                      context, FavoriteListView.routeName);
                },
                icon: const Icon(Icons.favorite)),
            IconButton(
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return BlocProvider.value(
                        value: movieListCubit, child: const SortFilterView());
                  }));
                },
                icon: const Icon(Icons.filter_list)),
            IconButton(
              icon: const Icon(Icons.settings),
              onPressed: () {
                Navigator.restorablePushNamed(context, SettingsView.routeName);
              },
            ),
          ],
        ),
        body: BlocBuilder<MovieListCubit, MovieListState>(
          builder: (context, state) {
            if (state.status.isLoading) {
              return Container(
                height: 350,
                alignment: Alignment.center,
                color: Colors.white,
                child: const CircularProgressIndicator(),
              );
            }

            if (state.movieList == null) {
              return Container(
                height: 350.0,
                padding: const EdgeInsets.symmetric(horizontal: 44.0),
                color: Colors.white,
                alignment: Alignment.center,
                child: const Text(
                  'Unable to Retrieve Movie List',
                  textAlign: TextAlign.center,
                ),
              );
            }
            return ListView.builder(
              // restorationId: 'movieListView',
              itemCount: state.movieList!.results!.length,
              itemBuilder: (BuildContext context, int index) {
                final movie = state.movieList!.results![index];
                final genreText = state.genreTextList![index];

                return MovieListItemWidget(
                  movie: movie,
                  genreText: genreText,
                );
              },
            );
          },
        ),
      ),
    );
  }
}

class MovieListItemWidget extends StatelessWidget {
  const MovieListItemWidget({
    Key? key,
    required this.movie,
    this.genreText = '',
  }) : super(key: key);

  final MovieListItem movie;
  final String genreText;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        await Navigator.pushNamed(
          context,
          MovieDetailsView.routeName,
          arguments: movie.id,
        );
      },
      child: Card(
        margin: const EdgeInsets.only(left: 14.0, right: 14.0, bottom: 6.0),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 180.0,
              child: CachedNetworkImage(
                width: 120,
                imageUrl:
                    '${kImageBaseUrl}w220_and_h330_face${movie.posterPath}',
                fit: BoxFit.cover,
                placeholder: (context, _) => const Icon(
                  Icons.image,
                  size: 100,
                  color: kGrey10,
                ),
                errorWidget: (context, _, error) => const Center(
                  child: Icon(
                    Icons.error,
                    size: 50,
                  ),
                ),
              ),
            ),
            Flexible(
              child: Container(
                margin: const EdgeInsets.only(left: 8.0),
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      movie.title,
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .copyWith(fontSize: 14.0),
                    ),
                    Text(
                      genreText,
                      style: Theme.of(context).textTheme.caption,
                    ),
                    Text(
                      'Popularity: ${movie.popularity}',
                      style: Theme.of(context).textTheme.caption,
                    ),
                    const Divider(thickness: 2.0),
                    Text(
                      movie.overview,
                      maxLines: 4,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context)
                          .textTheme
                          .bodyText2!
                          .copyWith(color: kTextGrey),
                    ),
                    const SizedBox(height: 6.0),
                  ],
                ),
              ),
            ),
            BlocProvider(
              create: (context) =>
                  FavoriteMovieCubit()..checkFavMovie(movie.id),
              child: Tooltip(
                  message: 'Mark as Favorite',
                  child: FavoriteButton(movie: movie)),
            ),
          ],
        ),
      ),
    );
  }
}
