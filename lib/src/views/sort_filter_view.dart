import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:select_form_field/select_form_field.dart';
import 'package:tmdb/src/bloc/movie_list_cubit.dart';

class SortFilterView extends StatefulWidget {
  const SortFilterView({Key? key}) : super(key: key);

  static const routeName = '/sort_filter';

  @override
  State<SortFilterView> createState() => _SortFilterViewState();
}

class _SortFilterViewState extends State<SortFilterView> {
  final List<Map<String, dynamic>> _items = [
    {
      'value': 'popularity.desc',
      'label': 'Popularity Descending',
    },
    {
      'value': 'popularity.asc',
      'label': 'Popularity Ascending',
    },
    {
      'value': 'release_date.desc',
      'label': 'Release Date Descending',
    },
    {
      'value': 'release_date.asc',
      'label': 'Release Date Ascending',
    },
    {
      'value': 'vote_count.desc',
      'label': 'Vote Count Descending',
    },
    {
      'value': 'vote_count.asc',
      'label': 'Vote Count Ascending',
    },
  ];

  String sortValue = '';
  DateTime? selectedStartDate;
  DateTime? selectedEndDate;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sort and Filter Movies'),
      ),
      body: Column(
        children: [
          Card(
            margin:
                const EdgeInsets.symmetric(horizontal: 22.0, vertical: 10.0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            child: Padding(
              padding: const EdgeInsets.all(18.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Sort',
                    style: Theme.of(context).textTheme.headline5,
                  ),
                  const Divider(
                    thickness: 4.0,
                  ),
                  SelectFormField(
                    type: SelectFormFieldType.dropdown, // or can be dialog
                    initialValue: '',
                    labelText: 'Sort Movie List by',
                    items: _items,
                    onChanged: (val) {
                      sortValue = val;
                    },
                  ),
                ],
              ),
            ),
          ),
          Card(
            margin: const EdgeInsets.symmetric(horizontal: 22.0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            child: Padding(
              padding: const EdgeInsets.all(18.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Filter',
                    style: Theme.of(context).textTheme.headline5,
                  ),
                  const Divider(
                    thickness: 4.0,
                  ),
                  Text(
                    'Primary Release Date Range',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2!
                        .copyWith(fontSize: 16),
                  ),
                  const SizedBox(height: 10),
                  Row(
                    children: [
                      Expanded(
                        child: InkWell(
                          onTap: () async {
                            final _pickedDate = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime(1900),
                              lastDate: DateTime(2125),
                            );

                            if (_pickedDate != null) {
                              setState(() {
                                selectedStartDate = _pickedDate;
                              });
                            }
                            print(
                                'Selected Date: ${selectedStartDate.toString()}');
                          },
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Container(
                                    margin: const EdgeInsets.only(
                                        left: 10, right: 10),
                                    child: const Icon(
                                      Icons.date_range,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  Text(
                                    (selectedStartDate != null)
                                        ? DateFormat.yMMMMEEEEd()
                                            .format(selectedStartDate!)
                                        : "Date From",
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                  )
                                ],
                              ),
                              const SizedBox(height: 4),
                              const Divider(thickness: 2),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(width: 10),
                    ],
                  ),
                  const SizedBox(height: 20),
                  Row(
                    children: [
                      Expanded(
                        child: InkWell(
                          onTap: () async {
                            final _pickedDate = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime(1900),
                              lastDate: DateTime(2125),
                            );

                            if (_pickedDate != null) {
                              setState(() {
                                selectedEndDate = _pickedDate;
                              });
                            }
                            print(
                                'Selected End Date: ${selectedEndDate!.toIso8601String()}');
                          },
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Container(
                                    margin: const EdgeInsets.only(
                                        left: 10, right: 10),
                                    child: const Icon(
                                      Icons.date_range,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  Text(
                                    (selectedEndDate != null)
                                        ? DateFormat.yMMMMEEEEd()
                                            .format(selectedEndDate!)
                                        : "Date To",
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                  )
                                ],
                              ),
                              const SizedBox(height: 4),
                              const Divider(thickness: 2),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(width: 10),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Container(
              width: double.infinity,
              margin:
                  const EdgeInsets.symmetric(horizontal: 22.0, vertical: 10.0),
              child: ElevatedButton(
                onPressed: () async {
                  Map<String, String> params = {};
                  if (sortValue.isNotEmpty) {
                    params.addAll({'sort_by': sortValue});
                  }

                  if (selectedStartDate != null) {
                    params.addAll({
                      'primary_release_date.gte':
                          selectedStartDate!.toIso8601String()
                    });

                    if (selectedEndDate != null) {
                      params.addAll({
                        'primary_release_date.lte':
                            selectedEndDate!.toIso8601String()
                      });
                    } else {
                      params.addAll({
                        'primary_release_date.lte':
                            DateTime.now().toIso8601String()
                      });
                    }
                  }

                  if (params.isNotEmpty) {
                    await context
                        .read<MovieListCubit>()
                        .getMovieList(params: params);
                  }

                  Navigator.pop(context);
                },
                child: const Text('Search'),
              )),
        ],
      ),
    );
  }
}
