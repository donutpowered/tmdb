import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tmdb/src/bloc/favorite_movie_cubit.dart';
import 'package:tmdb/src/models/fav_movie.dart';

class FavoriteButton extends StatelessWidget {
  const FavoriteButton({Key? key, required this.movie}) : super(key: key);

  final FavMovie movie;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FavoriteMovieCubit, FavoriteMovieState>(
      builder: (context, state) {
        context.read<FavoriteMovieCubit>().checkFavMovie(movie.id);

        if (state.status.isLoading) {}

        return GestureDetector(
          onTap: () async {
            await context
                .read<FavoriteMovieCubit>()
                .toggleFavMovie(movie, state.status.isFavorite);
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(
              state.status.isFavorite ? Icons.favorite : Icons.favorite_border,
              color: Colors.red,
              // size: Sizes.dimen_12.h,
            ),
          ),
        );
      },
    );
  }
}
