import 'dart:async';

import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class HomepageWebview extends StatelessWidget {
  static const routeName = '/homepage_webview';

  HomepageWebview({Key? key, required this.redirectUrl}) : super(key: key);

  final String redirectUrl;

  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Movie Website')),
      body: WebView(
        initialUrl: redirectUrl,
        javascriptMode: JavascriptMode.unrestricted,
        onWebViewCreated: (WebViewController webViewController) {
          _controller.complete(webViewController);
        },
      ),
    );
  }
}
